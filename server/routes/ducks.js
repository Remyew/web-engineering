const express = require('express')
const router = express.Router()

const mongoose = require('mongoose');
const Schema = mongoose.Schema


const duckSchema = new Schema({
  // Duck data
  name: String,
  description: String,
  image: String,
  price: Number
});
const Duck = mongoose.model('Duck', duckSchema);

router.get('/listDucks', async function (req, res) {
  console.log("Listing products")
  Duck.find()
    .exec(function (err, data) {
      if (err)
        return res.status(500).send(err)

      if (data === null)
        return res.status(404).send()

      res.status(200).json(data)
    });
})

module.exports = router