const express = require('express');

const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();

app.use(bodyParser.json());
app.use(cors());


// Connect to mongodb
mongoose.connect('mongodb://LordDrumcules:geji34njef_wcfejio32@localhost:27042/abschlussprojekt?authSource=admin', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', async function () {
  console.log("Mongoose connected!")
});

// Register routes
app.use("/", require('./routes/ducks'));
app.use('/image', express.static('static/images'));

app.listen(3000);

console.log("Server is running on port 3000")
