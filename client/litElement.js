/*
 * Lit-Element
 * The ducks are displayed using a Lit-Element
 */
import {LitElement, html} from "http://unpkg.com/lit-element/lit-element.js?module";

class Duck extends LitElement {
  static get properties() {
    return {
      image: {type: String},
      name: {type: String},
      description: {type: String},
      price: {type: String},
      page: {type: Number}
    };
  }

  render() {
    return html`
      <link rel="stylesheet" type="text/css" href="assets/scss/litElement.css"/>
      <div class='duck'>
        <div class='image' style='background-image: url(${this.image})'>
          <div></div>
        </div>
        <div class='name'>
          <div class='highlighted-text'>
            <span>
              ${this.name}
            </span>
          </div>
        </div>
        <div class='description'>
          <span>
            ${this.description}
          </span>
        </div>
        <div class='price' @click="${this.handleClick}">
          <div class='highlighted-text'>
            <span>
              ${this.price} €
            </span>
          </div>
          <div class='background' />
        </div>
       </div>`;
  }

  handleClick(event) {
    console.log(this.shadowRoot.childNodes)
    this.shadowRoot.childNodes[4].classList.remove('clicked')
    this.shadowRoot.childNodes[4].classList.add('clicked')

    store.addToCart({name: this.name, price: this.price})
    console.log(JSON.stringify(store.state))

    setTimeout(() => {
      this.shadowRoot.childNodes[4].classList.remove('clicked')
    }, 500)
  }
}

customElements.define('v-duck', Duck);