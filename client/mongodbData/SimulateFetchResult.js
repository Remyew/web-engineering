function mongoDataFetch(url) {
  const reqUrl = url.split('/').filter((urlPart, index) =>
    index >= 3
  ).reduce((previousValue, currentValue) => previousValue + "/" + currentValue, "")

  return new Promise((resolve, reject) => {
    if (reqUrl === "/listDucks") {
      const data = {
        json: () => Promise.resolve([
          {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be30d"
            },
            "name": "Mr. Quackers",
            "description": "In convallis neque nisl, nec placerat quam finibus eget.",
            "image": "./mongodbData/images/alaaf-300x300.png",
            "price": "2"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be30f"
            },
            "name": "Bob",
            "description": "Aenean fringilla interdum lectus.",
            "image": "./mongodbData/images/gym-300x300.png",
            "price": "3"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be310"
            },
            "name": "Mona Lisa",
            "description": "In convallis neque nisl, nec placerat quam finibus eget.",
            "image": "./mongodbData/images/mona-lisa-300x300.png",
            "price": "2"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be311"
            },
            "name": "Openhoff",
            "description": "Duis consequat lacus libero, ut commodo purus ultrices sed.",
            "image": "./mongodbData/images/openhoff-300x300.png",
            "price": "1"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be30e"
            },
            "name": "Jeffery McDuck",
            "description": "Duis consequat lacus libero, ut commodo purus ultrices sed.",
            "image": "./mongodbData/images/diver-300x300.png",
            "price": "1"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be312"
            },
            "name": "Schaeffler",
            "description": "Aenean fringilla interdum lectus.",
            "image": "./mongodbData/images/schaeffler-300x300.png",
            "price": "3"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be313"
            },
            "name": "Zufalls Paket",
            "description": "Duis consequat lacus libero, ut commodo purus ultrices sed.",
            "image": "./mongodbData/images/package-1-300x300.png",
            "price": "15"
          }, {
            "_id": {
              "$oid": "5f79f58d059d4d3a6e3be314"
            },
            "name": "Zufall Einzeln",
            "description": "Aenean fringilla interdum lectus.",
            "image": "./mongodbData/images/gym-300x300.png",
            "price": "2"
          }])
      }

      return resolve(data)
    } else {
      return reject(new Error("Cannot GET " + reqUrl))
    }
  })
}