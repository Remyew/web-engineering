<?php
session_start();

$json = json_decode(file_get_contents('./data.json'), true);
$salt = "VY8V2HwFg9HVZPNLK*gKNv.Ei!VqonsxCAbrnwZgyeyJGWYctB8U@oj3VFy4wWAptrsDkkUzTtRu2TKXPe2*";

if (isset($_POST["logout"])) {
    unset($_SESSION['email']);
    unset($_SESSION['password']);
    session_destroy();

    header("Location:" . $_SERVER['PHP_SELF']);
}

if (isset($_POST['email']) && isset($_POST['password'])) {
    $email = hash("sha512", $_POST["email"] . $salt);
    $password = hash("sha512", $_POST["password"] . $salt);

    if (isset($_POST['register'])) {
        if (!isset($json[$email])) {
            $json[$email]["password"] = $password;
            if (file_put_contents('./data.json', json_encode($json, true))) {
                $_SESSION['status'] = 'register-success';
            } else {
                $_SESSION['status'] = 'register-failure';
            }
        } else {
            $_SESSION['status'] = 'register-mail';
        }
    } else {
        if ($json[$email]["password"] == $password) {
            $_SESSION["email"] = $email;
            $_SESSION["password"] = $password;
            $_SESSION['status'] = 'login-success';
        } else {
            $_SESSION['status'] = 'login-failure';
        }
    }

    header("Location:" . $_SERVER['PHP_SELF']);
}
?>
<!DOCTYPE html>
<html xml:lang="de">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title> The duck shop </title>

  <link rel="stylesheet" type="text/css" href="assets/scss/style.css"/>
  <link rel="stylesheet" type="text/css" href="assets/scss/container.css"/>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
        rel="stylesheet">

  <script src="./node_modules/vue/dist/vue.min.js"></script>
  <script src="./mongodbData/SimulateFetchResult.js"></script>

  <script src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
  <script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
  <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>

  <link rel=icon href=./favicon.ico>
</head>
<body>
<?php
if (!isset($_POST['email']) && isset($_SESSION['status'])) {
    switch ($_SESSION['status']) {
        case 'register-success':
            echo "<script>alert('Erfolgreich registriert!')</script>";
            break;
        case 'register-failure':
            echo "<script>alert('Registration fehlgeschlagen!')</script>";
            break;
        case 'register-mail':
            echo "<script>alert('Diese Mail Adresse wird bereits verwendet!')</script>";
            break;
        case 'login-success':
            echo "<script>alert('Erfolgreich angemeldet!')</script>";
            $_SESSION['status'] = 'none';
            break;
        case 'login-failure':
            echo "<script>alert('Anmeldung fehlgeschlagen!')</script>";
            break;
    }
    $_SESSION['status'] = "";
}
?>

<div id="app">
  <div class="header">
    <div class="text">
      <span>
        Ihrem Badezimmer fehlt das gewisse <span class="text-highlight">Etwas?</span>
      </span>
    </div>
    <div ref="header"></div>
  </div>

  <div class="mb-5">
    <div class="highlighted-text">
    <span>
      The duck shop
    </span>
    </div>
    <div class="highlighted-text">
    <span>
      We have all the ducks!
    </span>
    </div>
  </div>

  <div class="container mb-8">
    <div class="row duck-container mb-3" style="justify-content: space-evenly">
    </div>
    <div class="row">
      <div class="col">
        <div ref="prevPage" onclick="changePage(prevPage)">
        </div>
      </div>
      <div class="col" style="flex-grow: 1"></div>
      <div class="col">
        <div ref="nextPage" onclick="changePage(nextPage)">
        </div>
      </div>
    </div>
  </div>

  <div class="container mb-8">
    <div class="row">
      <div class="col highlighted-text">
        <span>
          Melden sie sich jetzt an!
        </span>
      </div>
    </div>
    <div class="row register-row">
        <?php
        if (isset($_SESSION['email']) && isset($_SESSION['password']) && $json[$_SESSION['email']]["password"] == $_SESSION['password']) {
            ?>
          <div class="col">
            <div class="bordered">
              <form method="post">
                <div class="title mb-3">
                  Sie sind eingeloggt!
                </div>
                <div>
                  <input type="submit" name="logout" value="Ausloggen"/>
                </div>
              </form>
            </div>
          </div>
            <?php
        } else {
            ?>
          <div class="col">
            <div class="bordered">
              <form method="post">
                <div class="title">
                  Registrieren
                </div>
                <div class="mb-3">
                  <input type="text" name="email" placeholder="E-Mail"/>
                  <input type="text" name="password" placeholder="Password"/>
                </div>
                <input type="hidden" name="register"/>
                <input type="submit" value="Registrieren">
              </form>
            </div>
          </div>
          <div class="col">
            <div class="bordered">
              <form method="post">
                <div class="title">
                  Anmelden
                </div>
                <div class="mb-3">
                  <input type="text" name="email" placeholder="E-Mail"/>
                  <input type="password" name="password" placeholder="Passwort"/>
                </div>
                <input type="hidden" name="login"/>
                <input type="submit" value="Einloggen">
              </form>
            </div>
          </div>
            <?php
        }
        ?>
    </div>
  </div>
  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col" style="flex-grow: 1">
          <div>
            Badezimmer Bilder von
            <ul style="margin-top: 4px">
              <li>
                <a href="https://pixabay.com/de/users/jarmoluk-143740/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2094733">Michal
                  Jarmoluk</a> auf <a
                    href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2094733">Pixabay</a>
              </li>
              <li>
                <a href="https://unsplash.com/@sidekix"> Sidekix Media </a> auf <a
                    href="https://unsplash.com/photos/g51F6-WYzyU">unsplash.com</a>
              </li>
              <li>
                <a href="https://unsplash.com/@philhearing"> Phil Hearing </a> auf <a
                    href="https://unsplash.com/photos/U7PitHRnTNU">unsplash.com</a>
              </li>
            </ul>
          </div>
          <div>
            Cart Icon made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>
            from <a
                href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <cart ref="cart">
    <div id="cartProducts"></div>
  </cart>
</div>


<script src="./store.js"></script>
<script>
  /*
   * Vue component
   * This component displays the cart, by loading the items from the store
   */
  Vue.component('cart', {
    template: `
        <div class="cart">
          <div v-if="dialog" :key='updateVal' class="popup">
            <div class="border"></div>
            <div class="items">
              <div v-for='(item, index) in store.state.cart' :key="item.name + '-' + index" class="cartItem">
                <div class="name"> {{ item.name }} </div>
                <div class="price">
                  {{ item.amount }} mal, je {{ item.price }} € | Gesamt: {{ item.price * item.amount }} €
                </div>
                <hr />
              </div>
            </div>
            <div v-if="store.state.cart.length > 0" class="priceTotal mb-2">
              <hr />
              Gesamtpreis: {{ store.state.cart.map(item => item.price * item.amount).reduce((a, b) => a+b, 0) }} €
            </div>
            <div v-else class="priceTotal mb-2">
              Ihr Warenkorb ist leer.
            </div>
<?php
    /*
     * Combined Vue & PHP:
     * Show checkout button in cart, only if the user is logged in
     */
    if (isset($_SESSION['email']) &&
    isset($_SESSION['password']) &&
    $json[$_SESSION['email']]["password"] == $_SESSION['password']
    ) {
    ?>
            <div v-if="store.state.cart.length > 0" class="checkout" @click="store.state.cart = []">
  Checkout
              </div>
<?php
    } else {
    ?>
            <div >
  Zum bestellen bitte anmelden.
              </div>
<?php
    }
    ?>
          </div>
          <div class="button" @click="dialog=!dialog">
            <div>
              <img v-if="!dialog" src="./assets/img/shopping-cart.svg" alt="cart"/>
              <span v-else> &#10005; </span>
            </div>
          </div>
        </div>`,
    data() {
      return {
        dialog: false,
        updateVal: 0,
        store
      }
    },
    props: {
      content: String
    }
  });

  const vm = new Vue({
    el: "#app"
  });
</script>

<script src="./management.js"></script>
<script src="./litElement.js" type="module"></script>
<script src="react.js" type="text/babel"></script>
</body>
</html>