/*
 * WebApps: Store
 */
let store = {
  debug: true,
  state: {
    cart: [],
    ducks: [],
    page: -1,
    elementsPerPage: 3,
    pages: 3
  },
  addToCart(value) {
    const name = value.name
    const nameIndex = this.state.cart.map(item => item.name).indexOf(name)

    if (nameIndex >= 0) {
      this.state.cart[nameIndex].amount++
    } else {
      this.state.cart.push({...value, amount: 1})
    }
  },
  addDuck(value) {
    this.state.ducks.push(value)
  },
}

localStorage.getItem('store') && (store.state.cart = JSON.parse(localStorage.getItem('store')).cart)

// Keep cart on reload
window.onbeforeunload = function () {
  localStorage.setItem('store', JSON.stringify(store.state))
};