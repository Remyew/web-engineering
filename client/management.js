// Change the amount of products shown, based on the screen width
const onResize = () => {
  if (store.state.elementsPerPage === 2 && window.innerWidth >= 1020) {
    store.state.elementsPerPage = 3
    store.state.pages = Math.round((store.state.ducks.length / store.state.elementsPerPage) + 0.4)
    changePage(refresh)
  } else if (store.state.elementsPerPage === 3 && window.innerWidth < 1020) {
    store.state.elementsPerPage = 2
    store.state.pages = Math.round((store.state.ducks.length / store.state.elementsPerPage) + 0.4)
    changePage(refresh)
  }
};
// Call resize function whenever the screen resizes
window.addEventListener('resize', onResize);


/*
 * Async JS
 * Data needed to display the ducks are fetched asynchronously using a promise
 */
mongoDataFetch('http://localhost:3000/listDucks')
  // fetch('http://localhost:3000/listDucks')
  .then(response => response.json())
  .then(data => {
    /*
     * DOM API
     * For each object fetched, a display object is inserted via the JS DOM API.
     */
    data.forEach((duck) => {
      // Save each element in the store
      store.addDuck({
        name: duck.name,
        image: duck.image,
        description: duck.description,
        price: duck.price
      })
    })

    onResize()
    changePage(nextPage)
  });

/*
(async function() {
  const fetchResponse = await mongoDataFetch('http://localhost:3000/listDucks');
  // const fetchResponse = await fetch('http://localhost:3000/listDucks')
  const data = await fetchResponse.json();

  data.forEach((duck) => {
    // Save each element in the store
    store.addDuck({
      name: duck.name,
      image: duck.image,
      description: duck.description,
      price: duck.price
    })
  })

  onResize()
  changePage(nextPage)
})();
*/


/*
 * History api is used to render the store pages
 * On popstate: Clear page and rerender
 */
window.addEventListener('popstate', () => {
  document.querySelector('.duck-container').innerHTML = ""

  console.log("Popped; State is now: " + history.state)
  if (history.state != null)
    store.state.page = history.state

  changePage(refresh)
});


/*
 * changePage runs another function when called.
 * In this case either next or prev
 */
const isBetween = (val, min, max) => min <= val && max >= val

function changePage(changePage) {
  changePage()

  // Remove all elements from the container
  document.querySelector('.duck-container').innerHTML = ""

  // Load new elements from the store
  store.state.ducks.forEach((duck, index) => {
    if (isBetween(index, store.state.page * store.state.elementsPerPage, (store.state.page + 1) * store.state.elementsPerPage - 1)) {
      const duckElement = document.createElement("v-duck")
      duckElement.setAttribute('name', duck.name)
      duckElement.setAttribute('image', duck.image)
      duckElement.setAttribute('description', duck.description)
      duckElement.setAttribute('price', duck.price)

      duckElement.classList.add('duck')
      document.querySelector('.duck-container').appendChild(duckElement)
    }
  })
}

const nextPage = () => {
  store.state.page = (store.state.page + 1) % store.state.pages
  // Add store navigation to history
  console.log("Push: " + store.state.page)
  history.pushState(store.state.page, `Page ${store.state.page}`);
}
const prevPage = () => {
  store.state.page = (store.state.page + store.state.pages - 1) % store.state.pages
  // Add store navigation to history
  console.log("Push: " + store.state.page)
  history.pushState(store.state.page, `Page ${store.state.page}`);
}
const refresh = () => {
}