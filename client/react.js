/*
 * React component which displays the cart
 */

class NavigationArrow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isTriangle: false}
    this.toTriangle = React.createRef();
    this.toArrow = React.createRef();
  }

  handleClick = () => {
    if (this.state.isTriangle) {
      this.toArrow.current.beginElement()
    } else {
      this.toTriangle.current.beginElement()
    }
    this.setState({isTriangle: !this.state.isTriangle})
  }

  render() {
    return (
      <svg viewBox="0 0 222 222" width="24px" style={{transform: this.props.left && "scaleX(-1)", cursor: "pointer"}}
           onMouseEnter={this.handleClick} onMouseLeave={this.handleClick}>
        <polygon id="shape" points="
      0,97 164,97 91,22 110,0 222,111
      110,222 91,200 164,125 0,125"
        >
          <animate ref={this.toTriangle}
                   begin="indefinite"
                   fill="freeze"
                   attributeName="points"
                   dur="200ms"
                   to="
               0,0 55,28 111,56 166,83 222,111
               166,139 111,167 55,194 0,222"/>
          <animate ref={this.toArrow}
                   begin="indefinite"
                   fill="freeze"
                   attributeName="points"
                   dur="200ms"
                   to="
      0,97 164,97 91,22 110,0 222,111
      110,222 91,200 164,125 0,125"/>
        </polygon>
      </svg>
    );
  }
}

ReactDOM.render(<NavigationArrow/>, vm.$refs.nextPage)
ReactDOM.render(<NavigationArrow left/>, vm.$refs.prevPage)


class HeaderBackground extends React.Component {
  constructor(props) {
    super(props);
    this.state = {backgroundNumber: 0}
    this.loadingBar = React.createRef();

    setInterval(() => {
      this.nextBackground()
      this.loadingBar.current.beginElement()
    }, this.props.cycleDuration)
  }

  nextBackground = () => {
    this.setState({backgroundNumber: (this.state.backgroundNumber + 1) % this.props.backgrounds.length})
  }

  componentDidMount() {
    this.loadingBar.current.beginElement()
  }


  render() {
    return (
      <div class="background">
        {this.props.backgrounds.map((value, index) => {
          return <div key={index} style={{
            backgroundImage: `url(${value})`,
            opacity: index === this.state.backgroundNumber ? 1 : 0
          }}/>
        })}

        <svg viewBox="0 0 2000 2" xmlns="http://www.w3.org/2000/svg">
          <path strokeWidth="20" d={`M0 1 L0 1`}>
            <animate ref={this.loadingBar}
                     begin="indefinite"
                     fill="freeze"
                     attributeName="d"
                     dur={`${this.props.cycleDuration}ms`}
                     to="M0 1 L2100 1"/>
          </path>
        </svg>
      </div>
    );
  }
}

ReactDOM.render(<HeaderBackground
  backgrounds={['./assets/img/bathroom1.jpg', './assets/img/bathroom2.jpg', './assets/img/bathroom3.jpg']} cycleDuration={3000}/>, vm.$refs.header)